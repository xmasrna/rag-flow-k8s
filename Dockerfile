FROM swr.cn-north-4.myhuaweicloud.com/infiniflow/ragflow-base:v1.0 as src
USER  root

WORKDIR /ragflow

ADD ./web ./web
RUN cd ./web && npm i --force && npm run build

ADD ./api ./api
ADD ./conf ./conf
ADD ./deepdoc ./deepdoc
ADD ./rag ./rag
ADD docker/entrypoint.sh ./entrypoint.sh
RUN chmod +x ./entrypoint.sh

FROM scratch
COPY --from=src --chown=1000:1000 / /

WORKDIR /ragflow

ENV PYTHONPATH=/ragflow/
ENV HF_ENDPOINT=https://hf-mirror.com


ENTRYPOINT ["./entrypoint.sh"]